export interface TableDataModel {
    id: number;
    tableName: string;
    status: boolean;
}

export interface ReservationModel {
    name: string;
    phoneNumber: string;
    date: string;
    checkIn: string;
    checkOut: string;
    numberOfCustomers: number;
}

export function InitReservationData(): ReservationModel {
    return {
        name: '',
        phoneNumber: '',
        date: '',
        checkIn: '',
        checkOut: '',
        numberOfCustomers: 0,
    };
}