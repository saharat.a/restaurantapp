import React, { Component } from 'react';
import { Text, TouchableOpacity, View, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import { styles } from './styles';
import { TableDataModel } from '../../models/ReservationModel';

interface Props {
  isVisible: boolean;
  allTable: TableDataModel[];
  onCloseModal: Function;
}
interface State {
}
export default class CheckTableModal extends Component<Props, State> {

  constructor(public props: Props, public state: State) {
    super(props, state);
    this.state = {
    };
  }

  static defaultProps = {
    onCloseModal: () => { },
  };

  onCloseModal() {
    this.props.onCloseModal();
  }


  render() {
    const { isVisible, allTable } = this.props;
    return (
      <Modal
        isVisible={isVisible}
        onBackdropPress={this.props.onCloseModal}
      >
        <View style={styles.container}>
          <View style={styles.modalHeader}>
            <Text style={styles.messageHeader}>Available Table</Text>
          </View>
          <View style={styles.endLine} />
          <ScrollView>
            <View style={styles.messageBodyContainer}>
              {allTable.map(element => {
                if (element.status) {
                  return <View key={element.id} style={{ marginBottom: 10 }}>
                    <Text style={styles.detailStyle}>{element.tableName}</Text>
                  </View>;
                }
              })}
            </View>
          </ScrollView>
          <View style={styles.footer}>
            <TouchableOpacity style={styles.closeButton} onPress={() => this.onCloseModal()}>
              <Text style={[styles.closeButtonTittle, { color: '#AFBED5' }]}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}