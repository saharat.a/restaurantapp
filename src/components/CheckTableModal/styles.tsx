import { StyleSheet } from 'react-native';
import { ScreenUtils } from '../../commons/ScreenUtils';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    paddingTop: ScreenUtils.responsiveScale(48),
    paddingLeft: ScreenUtils.responsiveScale(17),
    paddingRight: ScreenUtils.responsiveScale(17),
    paddingBottom: ScreenUtils.responsiveScale(28),
  },
  modalHeader: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    position: 'relative',
  },
  messageHeader: {
    color: '#5F718E',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: ScreenUtils.responsiveScale(20),
  },
  messageBodyContainer: {
    height: ScreenUtils.responsiveScale(150),
    marginTop: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: ScreenUtils.responsiveScale(31),
  },
  button: {
    width: '100%',
    height: ScreenUtils.responsiveScale(45),
    backgroundColor: '#646BE8',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    fontWeight: 'bold',
    color: '#FFFFFF',
    fontSize: ScreenUtils.responsiveScale(13),
    height: ScreenUtils.responsiveScale(15),
  },
  closeButton: {
    marginTop: ScreenUtils.responsiveScale(10),
    backgroundColor: '#EEF2FA',
    borderRadius: ScreenUtils.responsiveScale(8),
    width: '100%',
    height: ScreenUtils.responsiveScale(45)
  },
  closeButtonTittle: {
    paddingTop: ScreenUtils.responsiveScale(15),
    fontSize: ScreenUtils.responsiveScale(16),
    textAlign: 'center',
    fontWeight: 'bold'
  },
  detailStyle: {
    fontSize: ScreenUtils.responsiveScale(16),
    color: '#5F718E',
    fontWeight: 'bold',
  },
  endLine: {
    borderBottomColor: 'silver',
    borderBottomWidth: 0.5,
    marginTop: ScreenUtils.responsiveScale(10),
    marginBottom: ScreenUtils.responsiveScale(10),
  },
});
