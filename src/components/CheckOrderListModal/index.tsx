import React, { Component } from 'react';
import { Text, TouchableOpacity, View, ScrollView, Alert } from 'react-native';
import Modal from 'react-native-modal';
import { styles } from './styles';
import { ReservationModel } from '../../models/ReservationModel';
import { ReservationService } from '../../services/apis/ReservationService';

interface Props {
    isVisible: boolean;
    allReservationOrder: ReservationModel[],
    onCloseModal: Function;
}
interface State {
}

export default class CheckOrderListModal extends Component<Props, State> {

    constructor(public props: Props, public state: State) {
        super(props, state);
        this.state = {
        };
    }

    static defaultProps = {
        onCloseModal: () => { },
    };

    onCloseModal() {
        this.props.onCloseModal();
    }

    async onclearOrder() {
        try {
            const defaultTable = [
                  { id: 1, tableName: 'Table-1', status: true },
                  { id: 2, tableName: 'Table-2', status: true },
                  { id: 3, tableName: 'Table-3', status: true },
                  { id: 4, tableName: 'Table-4', status: true },
                  { id: 5, tableName: 'Table-5', status: true },
                ];
            await ReservationService.setReservationOrder(JSON.stringify([]));
            await ReservationService.setDefaultTable(JSON.stringify(defaultTable));
        } catch (e) {
            Alert.alert('The error occur, please try again')
        } finally {
            this.props.onCloseModal();
        }
    }

    render() {
        const { isVisible, allReservationOrder } = this.props
        if (allReservationOrder.length === 0) {
            return <Modal
                isVisible={isVisible}
                onBackdropPress={this.props.onCloseModal}
            >
                <View style={styles.container}>
                    <View style={styles.modalHeader}>
                        <Text style={styles.messageHeader}>Order list</Text>
                    </View>
                    <View style={styles.endLine} />
                    <View style={styles.noHaveDataBodyContainer}>
                        <Text style={styles.messageHeader}>No have order</Text>
                    </View>
                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.closeButton} onPress={() => this.onCloseModal()}>
                            <Text style={[styles.closeButtonTittle, { color: '#AFBED5' }]}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        }
        return (
            <Modal
                isVisible={isVisible}
                onBackdropPress={this.props.onCloseModal}
            >
                <View style={styles.container}>
                    <View style={styles.modalHeader}>
                        <Text style={styles.messageHeader}>Order list</Text>
                    </View>
                    <View style={styles.endLine} />
                    <ScrollView>
                        <View style={styles.messageBodyContainer}>
                            {allReservationOrder.map((element, index) => {
                                return <View key={`${element.name}-${index}`} style={styles.orderDetailStyle}>
                                    <Text style={styles.orderHeader}>Order: {index + 1}</Text>
                                    <Text style={styles.detailStyle}>Name: {element.name}</Text>
                                    <Text style={styles.detailStyle}>Phone number: {element.phoneNumber}</Text>
                                    <Text style={styles.detailStyle}>Date: {element.date}</Text>
                                    <Text style={styles.detailStyle}>Check in: {element.checkIn}</Text>
                                    <Text style={styles.detailStyle}>Check out: {element.checkOut}</Text>
                                    <Text style={styles.detailStyle}>Number of customers: {element.numberOfCustomers}</Text>
                                </View>;
                            })}
                        </View>
                    </ScrollView>
                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.clearOrderButton} onPress={() => this.onclearOrder()}>
                            <Text style={[styles.closeButtonTittle, { color: 'white' }]}>Clear Order</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.closeButton} onPress={() => this.onCloseModal()}>
                            <Text style={[styles.closeButtonTittle, { color: '#AFBED5' }]}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}