import AsyncStorage from '@react-native-community/async-storage';
import { TableDataModel } from '../../models/ReservationModel';


enum StorageKey {
  AllTableData = '@MySuperStore:AllTableData',
  AllOrderData = '@MySuperStore:AllOrderData',
}
export class ReservationService {
  public static setDefaultTable(allTable: string): Promise<any> {
    return AsyncStorage.setItem(StorageKey.AllTableData,allTable);
  }

  public static getAllTable(): Promise<any> {
    return AsyncStorage.getItem(StorageKey.AllTableData).then(data => {
      if (!data) {
        return [];
      }
      return JSON.parse(data);
    });
  }

  public static getAllReservationOrder(): Promise<any> {
    return AsyncStorage.getItem(StorageKey.AllOrderData).then(data => {
      if (!data) {
        return [];
      }
      return JSON.parse(data);
    });
  }

  public static setReservationOrder(data:string): Promise<any> {
    return AsyncStorage.setItem(StorageKey.AllOrderData, data);
  }

}