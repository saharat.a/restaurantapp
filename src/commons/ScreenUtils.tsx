import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export class ScreenUtils {
    public static responsiveScale(size: number) {
        return width * 0.0025 * size;
    }
}
