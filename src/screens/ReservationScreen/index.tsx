import React, { Component, Fragment } from "react";
import { SafeAreaView, Text, View, Image, ScrollView, TouchableOpacity, TextInput, KeyboardAvoidingView, Platform, ImageBackground, Alert } from "react-native";
import { styles } from "./styles";
import { IMG_CARBONARA } from "../../Images";
import NavigationBar from "react-native-navbar";
import CheckTableModal from "../../components/CheckTableModal";
import { ReservationService } from "../../services/apis/ReservationService";
import { TableDataModel, ReservationModel, InitReservationData } from "../../models/ReservationModel";
import CheckOrderListModal from "../../components/CheckOrderListModal";



export interface Props { }

export interface State {
    name: string;
    phoneNumber: string;
    date: string;
    checkIn: string;
    checkOut: string;
    numberOfCustomers: number;
    isShowCheckTableModal: boolean;
    allTable: TableDataModel[];
    isShowCheckOrderModal: boolean;
    reservationModel: ReservationModel;
    allReservationOrder: ReservationModel[];
}

export default class ReservationScreen extends Component<Props, State> {
    constructor(public props: Props, public state: State) {
        super(props, state);
        this.state = {
            name: '',
            phoneNumber: '',
            date: '',
            checkIn: '',
            checkOut: '',
            numberOfCustomers: 0,
            isShowCheckTableModal: false,
            allTable: [],
            isShowCheckOrderModal: false,
            reservationModel: InitReservationData(),
            allReservationOrder: [],
        }
    }

    async componentDidMount() {
        const defaultTable = [
            { id: 1, tableName: 'Table-1', status: true },
            { id: 2, tableName: 'Table-2', status: true },
            { id: 3, tableName: 'Table-3', status: true },
            { id: 4, tableName: 'Table-4', status: true },
            { id: 5, tableName: 'Table-5', status: true },
        ];
        await ReservationService.setReservationOrder(JSON.stringify([]));
        await ReservationService.setDefaultTable(JSON.stringify(defaultTable))
        let allTable = await ReservationService.getAllTable()
        this.setState({ allTable })
    }

    async onMakeReservation() {
        const { name, phoneNumber, date, checkIn, checkOut, numberOfCustomers } = this.state;
        try {
            this.state.reservationModel.name = name;
            this.state.reservationModel.phoneNumber = phoneNumber;
            this.state.reservationModel.date = date;
            this.state.reservationModel.checkIn = checkIn;
            this.state.reservationModel.checkOut = checkOut;
            this.state.reservationModel.numberOfCustomers = numberOfCustomers;
            const getReservationOrder = await ReservationService.getAllReservationOrder();
            if (Array.isArray(getReservationOrder) && getReservationOrder.length > 0) {
                getReservationOrder.push(this.state.reservationModel)
                await ReservationService.setReservationOrder(JSON.stringify(getReservationOrder));
            } else {
                const data = [];
                data.push(this.state.reservationModel);
                await ReservationService.setReservationOrder(JSON.stringify(data));
            }
            await ReservationService.setDefaultTable(JSON.stringify(this.state.allTable.slice(Math.ceil(numberOfCustomers / 4))))
        } catch (e) {
            Alert.alert('The error occur, please try again')
        } finally {
            this.setState({ name: '', phoneNumber: '', date: '', checkIn: '', checkOut: '', numberOfCustomers: 0, reservationModel: InitReservationData(), allTable: this.state.allTable.slice(Math.ceil(numberOfCustomers / 4)) })
        }
    }

    renderMakeReservationButton() {
        const { name, phoneNumber, date, checkIn, checkOut, numberOfCustomers, allTable } = this.state;
        const checkDataBeforeSubmit = name && phoneNumber && date && checkIn && checkOut && Number(numberOfCustomers) !== 0 ? true : false;
        const availableTable = allTable.filter(element => { if (element) { return element.status === true } })
        const checkAvailableTable = availableTable.length * 4 >= numberOfCustomers && availableTable.length !== 0 ? true : false;
        if (!checkAvailableTable) {
            return <TouchableOpacity style={styles.notAvailableButton} disabled={true}>
                <Text style={[styles.submitButtonTittle, { color: 'white' }]}>No have avialable table</Text>
            </TouchableOpacity>;
        }
        if (checkDataBeforeSubmit) {
            return <TouchableOpacity style={styles.submitButton} onPress={() => this.onMakeReservation()}>
                <Text style={[styles.submitButtonTittle, { color: 'white' }]}>Make Reservation</Text>
            </TouchableOpacity>;
        }

        return <TouchableOpacity style={styles.submitButtonDisabled} disabled={true}>
            <Text style={[styles.submitButtonTittle, { color: '#AFBED5' }]}>Make Reservation</Text>
        </TouchableOpacity>;
    }

    async onShowCheckTableModal() {
        const allTable = await ReservationService.getAllTable();
        this.setState({ isShowCheckTableModal: true, allTable })
    }

    async onShowCheckOrderModal() {
        const allReservationOrder = await ReservationService.getAllReservationOrder();
        this.setState({ isShowCheckOrderModal: true, allReservationOrder })
    }

    onCloseCheckTableModal() {
        this.setState({ isShowCheckTableModal: false });
    }

    onCloseCheckOrderModal() {
        this.setState({ isShowCheckOrderModal: false });
    }

    render() {
        const { name, phoneNumber, date, checkIn, checkOut, numberOfCustomers, isShowCheckTableModal, allTable, isShowCheckOrderModal, allReservationOrder } = this.state;
        return (
            <Fragment>
                <SafeAreaView style={styles.flexContainer}>
                    <NavigationBar
                        style={styles.navStyle}
                        statusBar={{ style: 'default', hidden: true }}
                        title={{ title: 'Reservation' }}
                    />
                    <ScrollView>
                        <KeyboardAvoidingView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }} enabled behavior={Platform.OS === 'android' ? undefined : 'position'}>
                            <View style={styles.contentContainer}>
                                <View>
                                    <Image style={styles.imageStyle} source={IMG_CARBONARA}></Image>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Name</Text>
                                    <TextInput
                                        testID='nameField'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={name}
                                        onChangeText={name => this.setState({ name })}
                                        placeholder='Mr. test'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Phone number</Text>
                                    <TextInput
                                        testID='phoneNummberField'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={phoneNumber}
                                        keyboardType={'numeric'}
                                        maxLength={10}
                                        onChangeText={phoneNumber => this.setState({ phoneNumber })}
                                        placeholder='08-123-xxx'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Date</Text>
                                    <TextInput
                                        testID='dateField'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={date}
                                        onChangeText={date => this.setState({ date })}
                                        placeholder='12/12/20'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Check in</Text>
                                    <TextInput
                                        testID='checkInField'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={checkIn}
                                        onChangeText={checkIn => this.setState({ checkIn })}
                                        placeholder='12:00 AM'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Check out</Text>
                                    <TextInput
                                        testID='checkOutField'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={checkOut}
                                        onChangeText={checkOut => this.setState({ checkOut })}
                                        placeholder='2:00 PM'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                                <View style={styles.inputWrapper}>
                                    <Text style={styles.inputLabel}>Number of customers</Text>
                                    <TextInput
                                        testID='Field'
                                        style={styles.inputField}
                                        autoCapitalize='none'
                                        autoCompleteType='off'
                                        returnKeyType='done'
                                        autoCorrect={false}
                                        value={numberOfCustomers}
                                        keyboardType={'numeric'}
                                        onChangeText={numberOfCustomers => this.setState({ numberOfCustomers })}
                                        placeholder='4 persons per table'
                                        placeholderTextColor='#CCD5E3'
                                    />
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                        <View style={[styles.submitBtnContainer, styles.contentContainer]}>
                            <TouchableOpacity style={styles.checkTableButtton} onPress={() => this.onShowCheckTableModal()}>
                                <Text style={[styles.submitButtonTittle, { color: 'white' }]}>Check Table</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.checkOrderButtton} onPress={() => this.onShowCheckOrderModal()}>
                                <Text style={[styles.submitButtonTittle, { color: 'white' }]}>Check Order</Text>
                            </TouchableOpacity>
                            {this.renderMakeReservationButton()}
                        </View>
                        <CheckTableModal
                            isVisible={isShowCheckTableModal}
                            allTable={allTable}
                            onCloseModal={() => this.onCloseCheckTableModal()}
                        />
                        <CheckOrderListModal
                            isVisible={isShowCheckOrderModal}
                            allReservationOrder={allReservationOrder}
                            onCloseModal={() => this.onCloseCheckOrderModal()}
                        />
                    </ScrollView>
                </SafeAreaView>
            </Fragment>
        );
    }
}
