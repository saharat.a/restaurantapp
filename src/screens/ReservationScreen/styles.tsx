import { StyleSheet } from 'react-native'
import { ScreenUtils } from '../../commons/ScreenUtils';

export const styles = StyleSheet.create({
    flexContainer: {
        flex: 1,
    },
    contentContainer: {
        height: '100%',
        marginLeft: ScreenUtils.responsiveScale(16),
        flex: 1,
        paddingTop: '7%',
        paddingBottom: '7%',
        paddingLeft: '2%',
        paddingRight: '4%',
    },
    topicStyle: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: ScreenUtils.responsiveScale(16),
        lineHeight: ScreenUtils.responsiveScale(24),
        color: '#4F5E76',
        paddingBottom: ScreenUtils.responsiveScale(20),
    },
    topicDetailStyle: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: ScreenUtils.responsiveScale(10),
        lineHeight: ScreenUtils.responsiveScale(15),
        color: '#4F5E76'
    },
    detailStyle: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: ScreenUtils.responsiveScale(10),
        lineHeight: ScreenUtils.responsiveScale(15),
        color: '#4F5E76',
        marginBottom: '5%',
    },
    inputLabel: {
        color: '#667CA0',
        fontSize: ScreenUtils.responsiveScale(16),
        fontWeight: 'bold',
        marginTop: ScreenUtils.responsiveScale(10),
        marginBottom: ScreenUtils.responsiveScale(5),
      },
      messageWarning: {
        color: '#E26880',
        fontSize: ScreenUtils.responsiveScale(12),
        height: ScreenUtils.responsiveScale(18),
        marginTop: ScreenUtils.responsiveScale(7),
      },
      inputField: {
        color: '#414B5A',
        marginTop: ScreenUtils.responsiveScale(10),
        fontSize: ScreenUtils.responsiveScale(15),
        height: ScreenUtils.responsiveScale(30),
        borderBottomColor: '#CCD5E3',
        borderBottomWidth: 1,
        paddingBottom: ScreenUtils.responsiveScale(10),
        fontWeight: '500',
      },
      inputWrapper: {
        marginTop: ScreenUtils.responsiveScale(5),
      },
      submitBtnContainer: {
        width: '95%',
        justifyContent: 'center',
        alignItems: 'center',
      },
      submitButton: {
        marginTop: ScreenUtils.responsiveScale(10),
        backgroundColor: '#52D017',
        borderRadius: ScreenUtils.responsiveScale(8),
        width: '100%',
        height: ScreenUtils.responsiveScale(45)
      },
      submitButtonTittle: {
        paddingTop: ScreenUtils.responsiveScale(15),
        fontSize: ScreenUtils.responsiveScale(16),
        textAlign: 'center',
        fontWeight: 'bold'
      },
      submitButtonDisabled: {
        marginTop: ScreenUtils.responsiveScale(20),
        backgroundColor: '#EEF2FA',
        borderRadius: ScreenUtils.responsiveScale(8),
        width: '100%',
        height: ScreenUtils.responsiveScale(45)
      },
      imageStyle: {
        width: '100%',
        height: ScreenUtils.responsiveScale(250),
      },
      checkTableButtton: {
        marginTop: ScreenUtils.responsiveScale(10),
        backgroundColor: '#43C6DB',
        borderRadius: ScreenUtils.responsiveScale(8),
        width: '100%',
        height: ScreenUtils.responsiveScale(45)
      },
      checkOrderButtton: {
        marginTop: ScreenUtils.responsiveScale(10),
        backgroundColor: '#FA8072',
        borderRadius: ScreenUtils.responsiveScale(8),
        width: '100%',
        height: ScreenUtils.responsiveScale(45)
      },
      navStyle: {
        borderBottomColor: 'silver',
        borderBottomWidth: 0.5,
      },
      notAvailableButton: {
        marginTop: ScreenUtils.responsiveScale(10),
        backgroundColor: '#DC143C',
        borderRadius: ScreenUtils.responsiveScale(8),
        width: '100%',
        height: ScreenUtils.responsiveScale(45)
    },
})