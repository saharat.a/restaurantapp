import React from 'react';
import ReservationScreen from './screens/ReservationScreen';

interface props { }

interface state { }

export default class App extends React.Component {
  renderApp() {
      return <ReservationScreen />;
  }

  render() {
    const container = this.renderApp();
    return (
        <>
          {container}
        </>
    );
  }
}
